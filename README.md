> Stack for Amy

## Client

- [Vue](vuejs.org)
- [Sass](http://sass-lang.com)


## Server

- [Flask](http://flask.pocoo.org)
- [Bottle](http://bottlepy.org) 🦊 🐦
- [Django Rest](http://www.django-rest-framework.org)
- [Express](http://expressjs.com)
- [Phoenix](http://phoenixframework.org)
- [Rails](http://rubyonrails.org)
- [Sintra](http://sinatrarb.com)
---
- [Docker](https://docker.com) 🦊
- [Kubernetes](https://kubernetes.io)
- [MinShift](https://github.com/MiniShift/minishift)
- [Ansible](https://www.ansible.com)
- [Keycloak](https://www.keycloak.org) 🦊
---
- [Swagger](https://swagger.io) 🐦

## Machine Learning

- [Tensorflow](http://tensorflow.org) 
- [PyTorch](http://pytorch.org)

## NAtural Language

- [NLP.js](https://github.com/axa-group/nlp.js)
- [nltk](http://www.nltk.org)
- [spaCy](https://spacy.io)

## Database

- [Mongo](https://www.mongodb.com)
- [Redis](https://redis.io)
- [Postgres](https://postgresql.org)
- [RabbitMQ](http://www.rabbitmq.com) 🦊

## Plugins

- [Python](https://python.org) 🦊 🐦
- [Node](https://nodejs.org) 🐦
- [Ballerina](https://ballerina.io)
- [Go](https://www.golang.org)
